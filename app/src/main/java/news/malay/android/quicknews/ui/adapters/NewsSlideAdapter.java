package news.malay.android.quicknews.ui.adapters;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import news.malay.android.quicknews.parser.Article;
import news.malay.android.quicknews.ui.fragments.NewsDetailsFragment;


/**
 * Created by yogeshpatel on 04/09/17.
 */

public class NewsSlideAdapter extends FragmentStatePagerAdapter {

    private List<Article> articleList;
    public NewsSlideAdapter(FragmentManager fm, List<Article> articleList) {
        super(fm);
        this.articleList = articleList;
    }

    @Override
    public Fragment getItem(int position) {
        return NewsDetailsFragment.create(articleList.get(position));
    }

    @Override
    public int getCount() {
        return articleList.size();
    }
    public int getItemPosition(Object object){
        if(object!=null && object instanceof NewsDetailsFragment) {
            NewsDetailsFragment f = (NewsDetailsFragment) object;
            if (f != null) {
                f.updateUI();
            }
        }
        int position = articleList.indexOf(object);

        if (position >= 0) {
            return position;
        } else {
            return POSITION_NONE;
        }

    }
    public void dataSetChanged(List<Article> updatedArticleList){
        articleList = updatedArticleList;
        notifyDataSetChanged();
    }

}
