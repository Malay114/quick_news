package news.malay.android.quicknews.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import java.util.Set;

import news.malay.android.quicknews.QuickNewsSharedPref;
import news.malay.android.quicknews.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {

    private QuickNewsSharedPref quickNewsSharedPref;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        final String indiaStr = getActivity().getResources().getString(R.string.india);
        final String techStr = getActivity().getResources().getString(R.string.tech);
        final String worldStr = getActivity().getResources().getString(R.string.world);
        final String politicsStr = getActivity().getResources().getString(R.string.politics);
        final String sportsStr = getActivity().getResources().getString(R.string.sports);
        final String scienceStr = getActivity().getResources().getString(R.string.science);

        quickNewsSharedPref = new QuickNewsSharedPref(getActivity());
        Set<String> categorySet = quickNewsSharedPref.getSelectedCategory();


        ToggleButton indiaToggle = (ToggleButton) view.findViewById(R.id.toggle_india);

        indiaToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                if(state) {
                    quickNewsSharedPref.addCategory(indiaStr);
                }else {
                    quickNewsSharedPref.removeCategory(indiaStr);
                }
            }

        });
        if(categorySet.contains(indiaStr)){
            indiaToggle.setChecked(true);
        }

        ToggleButton politicsToggle = (ToggleButton) view.findViewById(R.id.toggle_politics);
        politicsToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                if(state) {
                    quickNewsSharedPref.addCategory(politicsStr);
                }else {
                    quickNewsSharedPref.removeCategory(politicsStr);
                }
            }
        });

        if(categorySet.contains(politicsStr)){
            politicsToggle.setChecked(true);
        }

        ToggleButton sportToggle = (ToggleButton) view.findViewById(R.id.toggle_sports);
        sportToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                if(state) {
                    quickNewsSharedPref.addCategory(sportsStr);
                }else {
                    quickNewsSharedPref.removeCategory(sportsStr);
                }

            }
        });
        if(categorySet.contains(sportsStr)){
            sportToggle.setChecked(true);
        }

        ToggleButton techToggle = (ToggleButton) view.findViewById(R.id.toggle_tech);
        techToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {

                if(state) {
                    quickNewsSharedPref.addCategory(techStr);
                }else {
                    quickNewsSharedPref.removeCategory(techStr);
                }
            }
        });

        if(categorySet.contains(techStr)){
            techToggle.setChecked(true);
        }

        ToggleButton worldToggle = (ToggleButton) view.findViewById(R.id.toggle_world);
        worldToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                if(state) {
                    quickNewsSharedPref.addCategory(worldStr);
                }else {
                    quickNewsSharedPref.removeCategory(worldStr);
                }

            }
        });

        if(categorySet.contains(worldStr)){
            worldToggle.setChecked(true);
        }

        ToggleButton scienceToggle = (ToggleButton) view.findViewById(R.id.toggle_science);
        scienceToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean state) {
                if(state) {
                    quickNewsSharedPref.addCategory(scienceStr);
                }else {
                    quickNewsSharedPref.removeCategory(scienceStr);
                }
            }
        });

        if(categorySet.contains(scienceStr)){
            scienceToggle.setChecked(true);
        }

        return  view;
    }

}
