package news.malay.android.quicknews.parser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import news.malay.android.quicknews.utils.quicknewsutils.QuickNewsUtils;

/**
 * Created by yogeshpatel on 05/09/17.
 */

public class FindSourceParser {

    private final XmlPullParser xmlParser;
    public FindSourceParser() {

        XmlPullParser parser = null;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(false);
            parser = factory.newPullParser();
        }
        catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        xmlParser = parser;
    }

    public String findNewsSource(String rssStream){

        try {
            // Get InputStream from String and set it to our XmlPullParser
            InputStream input = new ByteArrayInputStream(rssStream.getBytes());
            xmlParser.setInput(input, null);

            // Reuse Article object and event holder
            Article article = new Article();
            int eventType = xmlParser.getEventType();

            // Loop through the entire xml feed
            boolean needTobreakWhile =false;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = xmlParser.getName();
                //Log.e("tagname : ",tagname);

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        String src = handleNode(tagname);
                        if(src != null && src.length() > 0){
                            return src;
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("description")){
                            needTobreakWhile = true;
                            break;
                        }
                }
                if(needTobreakWhile){
                    break;
                }
                eventType = xmlParser.next();
            }
        }
        catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  "";
    }
    /**
     * Handles a node from the tag node and assigns it to the correct article value.
     * @param tag The tag which to handle.

     * @return True if a proper tag was given or handled. False if improper tag was given or
     * if an exception if triggered.
     */
    private String handleNode(String tag) {

        try {
            if(xmlParser.next() != XmlPullParser.TEXT) {

                return "";
            }

            if (tag.equalsIgnoreCase("title")) {

                String title = xmlParser.getText();
                return  QuickNewsUtils.newsSourceName (title);
            }
            if (tag.equalsIgnoreCase("description")) {

                String description = xmlParser.getText();
                return  QuickNewsUtils.newsSourceName (description);
            }


        }
        catch (IOException e) {
            e.printStackTrace();

        }
        catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        return  "";
    }
}
