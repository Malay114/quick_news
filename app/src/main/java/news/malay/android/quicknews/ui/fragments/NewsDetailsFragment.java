package news.malay.android.quicknews.ui.fragments;


import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.facebook.imagepipeline.request.Postprocessor;

import jp.wasabeef.fresco.processors.BlurPostprocessor;
import news.malay.android.quicknews.parser.Article;
import news.malay.android.quicknews.utils.image.ImageUtilites;
import news.malay.android.quicknews.utils.quicknewsutils.QuickNewsUtils;
import news.malay.android.quicknews.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewsDetailsFragment extends Fragment {


    private SimpleDraweeView newsImageView;
    private TextView titleTextView;
    private TextView moreInfoTextView;
    private TextView srcNameAndTime;
    private TextView timeTextView;

    private TextView descriptionTextView;
    private SimpleDraweeView newsSrcInformationImage;
    private ImageUtilites imageUtilites;

    public static final String ARTICLE = "article";
    private Article article;

    private Postprocessor postprocessor ;

    public NewsDetailsFragment() {
        // Required empty public constructor
    }

    public static NewsDetailsFragment create(Article article) {
        NewsDetailsFragment fragment = new NewsDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARTICLE, article);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postprocessor = new BlurPostprocessor(getActivity().getApplicationContext(),50);
        article = getArguments().getParcelable(ARTICLE);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_details, container, false);


        imageUtilites = new ImageUtilites(getActivity());
        newsImageView = (SimpleDraweeView) view.findViewById(R.id.src_image);

        titleTextView = (TextView) view.findViewById(R.id.title);
        descriptionTextView = (TextView) view.findViewById(R.id.description);
        srcNameAndTime = (TextView) view.findViewById(R.id.src_name_and_time);
        timeTextView = (TextView) view.findViewById(R.id.time_tv);
        newsSrcInformationImage = (SimpleDraweeView)view.findViewById(R.id.news_source_information_image);
        moreInfoTextView = (TextView) view.findViewById(R.id.news_source_information);
        moreInfoTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        updateUI();

        return  view;

    }

    public void updateUI(){
        moreInfoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = article.getSource(); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        String day = QuickNewsUtils.convertTodayOrYesterday(article.getDate());
        String sourceOfNews = article.getSourceTitle();

        srcNameAndTime.setText(sourceOfNews + " / " + day);
        timeTextView.setText(QuickNewsUtils.formateDateToView(article.getDate()));
        titleTextView.setText(article.getTitle());
        descriptionTextView.setText(article.getDescription());
    }

    @Override
    public void onStart() {
        super.onStart();

        Uri uri= article.getImage();

        if(uri != null) {

            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(article.getImage())
                    .setLocalThumbnailPreviewsEnabled(true)
                    .build();

            DraweeController controllerNews = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imageRequest)
                    .setOldController(newsImageView.getController())
                    .build();



            newsImageView.setController(controllerNews);
        }


    }


}
