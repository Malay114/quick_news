package news.malay.android.quicknews.parser;

/**
 * Created by yogeshpatel on 02/09/17.
 */

public class NewsSourceParserFactory {


    public NewsSource getNewsSource(String title){

        NewsSource newsSource = null ;
        if(title.toLowerCase().contains(NewsSourceNames.PHYS_ORG.toLowerCase())){
            newsSource = new PhysOrgNewsSourceImpl();
        }
        else if(title.toLowerCase().contains(NewsSourceNames.HINDUSTAN_TIMES.toLowerCase())){
            newsSource = new HindustanTimesSourceImpl();
        }
        else if(title.toLowerCase().contains(NewsSourceNames.BBC.toLowerCase())){
            newsSource = new BbcNewSourceImpl();
        }
        else if(title.toLowerCase().contains(NewsSourceNames.NEWS18.toLowerCase())) {
            newsSource = new News18NewsSourceImpl();
        }
        else if(title.toLowerCase().contains(NewsSourceNames.INDIA_TV_NEWS.toLowerCase())){
            newsSource  = new IndiaTvNewsSourceImpl();
        }
        else if(title.toLowerCase().contains(NewsSourceNames.FIRST_POST.toLowerCase())){
            newsSource = new FirstPostNewsSourceImpl();
        }
        else if(title.toLowerCase().contains(NewsSourceNames.SCIENCE_DAILY.toLowerCase())){
            newsSource = new ScienceDailyNewsSource();
        }
        return newsSource;
    }


}
