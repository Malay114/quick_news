package news.malay.android.quicknews;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import news.malay.android.quicknews.database.FeedReaderDbHelper;
import news.malay.android.quicknews.parser.Article;
import news.malay.android.quicknews.parser.FindSourceParser;
import news.malay.android.quicknews.parser.NewsSource;
import news.malay.android.quicknews.parser.NewsSourceParserFactory;

public abstract class BaseActivity extends AppCompatActivity {

    private FeedReaderDbHelper dbHelper;
    private FindSourceParser findSourceParser = new FindSourceParser();
    private int totalURLs;
    private int currentURLNo = 0;
    private Map<String, List<String>> categoryWithUrl = new HashMap<>();
    private Set<String> selectedCategory;
    private Set<String> allCategory;
    protected QuickNewsSharedPref sharedPref;

    abstract void dataLoaded();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        sharedPref = new QuickNewsSharedPref(this);
        dbHelper = new FeedReaderDbHelper(this);
        selectedCategory = sharedPref.getSelectedCategory();

        String indiaStr = getResources().getString(R.string.india);
        String techStr = getResources().getString(R.string.tech);
        String worldStr = getResources().getString(R.string.world);
        String politicsStr = getResources().getString(R.string.politics);
        String sportsStr = getResources().getString(R.string.sports);
        String scienceStr = getResources().getString(R.string.science);

        allCategory = new HashSet<>();
        allCategory.add(indiaStr);
        allCategory.add(techStr);
        allCategory.add(worldStr);
        allCategory.add(politicsStr);
        allCategory.add(sportsStr);
        allCategory.add(scienceStr);
    }

    protected void loadUrls() {


        dbHelper.deleteAllArticles();

        String[] general = getResources().getStringArray(R.array.feed_urls);
        String[] tech = getResources().getStringArray(R.array.tech);
        String[] world = getResources().getStringArray(R.array.world);
        String[] politics = getResources().getStringArray(R.array.politics);
        String[] sports = getResources().getStringArray(R.array.sports);
        String[] science = getResources().getStringArray(R.array.science);

        String indiaStr = getResources().getString(R.string.india);
        String techStr = getResources().getString(R.string.tech);
        String worldStr = getResources().getString(R.string.world);
        String politicsStr = getResources().getString(R.string.politics);
        String sportsStr = getResources().getString(R.string.sports);
        String scienceStr = getResources().getString(R.string.science);

        populateMapWithCategoryAndURL(indiaStr, general);
        populateMapWithCategoryAndURL(techStr, tech);
        populateMapWithCategoryAndURL(worldStr, world);
        populateMapWithCategoryAndURL(politicsStr, politics);
        populateMapWithCategoryAndURL(sportsStr, sports);
        populateMapWithCategoryAndURL(scienceStr, science);
        new LoadURLsAsync().execute();
    }


    private void populateMapWithCategoryAndURL(String category, String[] stringArr) {
        if (selectedCategory == null ||
                selectedCategory.size() <= 0 ||
                selectedCategory.contains(category)) {

            List<String> urlList = new ArrayList<>();
            for (String urls : stringArr) {

                urlList.add(urls);
            }
            totalURLs += urlList.size();

            categoryWithUrl.put(category, urlList);
        }
    }

    private void makeRequest(final String category, String url) {

        StringRequest req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                processData(category, response);

                            }
                        }).start();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();
                        currentURLNo++;
                        if (currentURLNo >= totalURLs) {
                            dataLoaded();
                        }
                    }
                }
        );
        QuickNewsApplication.getInstance().addToRequestQueue(req);
    }

    protected synchronized void processData(String category, String response) {
        currentURLNo++;
        String source = findSourceParser.findNewsSource(response);


        NewsSourceParserFactory factory = new NewsSourceParserFactory();
        NewsSource newsSource = factory.getNewsSource(source);

        if (newsSource != null && dbHelper != null) {
            List<Article> articleList = newsSource.parse(response, category);
            if (articleList.size() > 25) {
                for (int i = 0; i < 25; i++) {
                    dbHelper.addArticle(articleList.get(i));
                }
            } else {
                for (Article article : articleList) {
                    dbHelper.addArticle(article);
                }
            }
        }
        if (currentURLNo >= totalURLs) {
            dataLoaded();
        }


    }

    protected List<Article> getNewsOfSelectedCategory(Set<String> selectedCategory) {
        if (selectedCategory.size() <= 0) {
            selectedCategory = allCategory;
        }
        List<Article> articleList = dbHelper.getAllArticlesWithCategory(selectedCategory);
        return articleList;

    }

    private class LoadURLsAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (Map.Entry<String, List<String>> entry : categoryWithUrl.entrySet()) {
                        String category = entry.getKey();
                        List<String> urlList = entry.getValue();
                        for (String url : urlList) {
                            Log.e(category, url);
                            makeRequest(category, url);
                        }
                    }
                }
            }).start();

            return null;
        }


    }


}
