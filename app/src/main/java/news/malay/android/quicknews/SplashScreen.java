package news.malay.android.quicknews;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Date;

import news.malay.android.quicknews.utils.quicknewsutils.QuickNewsUtils;

public class SplashScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        TextView versionTv= (TextView)findViewById(R.id.version);
        versionTv.setText(BuildConfig.VERSION_NAME);

        if(isNetworkAvailable() && needToUpdateNews()) {
             super.loadUrls();

        }else{
            startMainActivity();
        }

    }

    private void startMainActivity(){
        Intent i = new Intent(SplashScreen.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    /**
     * This function will check whether network is available or not.
     *
     * @return
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * This function will check whether NEWS needs to be updated or not
     * it will find out the difference between last updated date and current date.
     * If hours is more than one hour then it will retur true else false.
     *
     * @return
     */
    private boolean needToUpdateNews(){
        long lastUpdatedTime = sharedPref.getLastUpatedTime();
        Date currentDate = new Date();

        if(lastUpdatedTime != 0){
            Date lastUpdatedDate = new Date(lastUpdatedTime);
            long diff = QuickNewsUtils.dateDiffWithCurDateInHours(lastUpdatedDate,currentDate);
            if(diff < 1){
                return false;
            }
            else {
                sharedPref.setLastUpdatedTime(currentDate.getTime());
                return true;
            }
        }
        sharedPref.setLastUpdatedTime(currentDate.getTime());

        return true;

    }

    @Override
    void dataLoaded() {
        startMainActivity();
    }
}
