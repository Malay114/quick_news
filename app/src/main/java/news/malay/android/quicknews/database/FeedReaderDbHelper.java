package news.malay.android.quicknews.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import news.malay.android.quicknews.parser.Article;
import news.malay.android.quicknews.utils.quicknewsutils.QuickNewsUtils;

/**
 * Created by yogeshpatel on 07/09/17.
 */

public class FeedReaderDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "FeedReader.db";

    // Contacts table name
    private static final String TABLE_FEEDS = "feeds";

    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";
    private static final String KEY_CONTENT = "content";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_TITLE = "title";
    private static final String KEY_IMAGE = "image";
    private static final String KEY_SOURCE = "source";
    private static final String KEY_TAGS = "tags";
    private static final String KEY_SOURCE_TITLE = "sourcetitle";
    private static final String KEY_CATEGORY = "category";


    public FeedReaderDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        String CREATE_FEEDS_TABLE = "CREATE TABLE " + TABLE_FEEDS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_DATE + " LONG,"
                + KEY_CONTENT + " TEXT,"
                + KEY_DESCRIPTION + " TEXT,"
                + KEY_IMAGE + " TEXT,"
                + KEY_TITLE + " TEXT,"
                + KEY_SOURCE + " TEXT,"
                + KEY_TAGS + " TEXT,"
                + KEY_CATEGORY + " TEXT COLLATE NOCASE,"
                + KEY_SOURCE_TITLE + " TEXT" +
                ")";
        db.execSQL(CREATE_FEEDS_TABLE);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        //db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addArticle(Article article){

        if(!QuickNewsUtils.isValidArticle(article)) {
            return;
        }
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_DATE,article.getDate());
        values.put(KEY_CONTENT,String.valueOf(article.getContent()));
        values.put(KEY_DESCRIPTION,String.valueOf(article.getDescription()));
        values.put(KEY_IMAGE,String.valueOf(article.getImage()));
        values.put(KEY_TITLE,String.valueOf(article.getTitle()));
        values.put(KEY_SOURCE,String.valueOf(article.getSource()));
        values.put(KEY_TAGS, QuickNewsUtils.stringListToString(article.getTags()));
        values.put(KEY_CATEGORY,String.valueOf(article.getCategory()));
        values.put(KEY_SOURCE_TITLE,String.valueOf(article.getSourceTitle()));

        db.insert(TABLE_FEEDS, null, values);
        db.close();

    }
    public List<Article> getAllArticles(){

        List<Article> articleList = new ArrayList<Article>();
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_FEEDS ;

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                Article article = new Article();
                article.setDate(Long.valueOf(cursor.getString(1)));
                article.setContent(cursor.getString(2));
                article.setDescription(cursor.getString(3));
                article.setImage(Uri.parse(cursor.getString(4)));
                article.setTitle(cursor.getString(5));
                article.setSource(Uri.parse(cursor.getString(6)));
                article.setTags(QuickNewsUtils.csvToList(cursor.getString(7)));
                article.setCategory(cursor.getString(8));
                article.setSourceTitle(cursor.getString(9));
                articleList.add(article);

            } while (cursor.moveToNext());
        }

        return articleList;
    }
    public List<Article> getAllArticlesWithCategory(Set<String> categorySet){


        List<String> categoryList = new ArrayList<>();
        categoryList.addAll(categorySet);

        List<Article> articleList = new ArrayList<Article>();
        SQLiteDatabase db = this.getWritableDatabase();

        StringBuilder builder = new StringBuilder();
        builder.append(" WHERE ").append(KEY_CATEGORY).append(" IN ");
        builder.append("(");
        for(int i = 0 ; i < categoryList.size() ; i++){

            builder.append("\'"+categoryList.get(i)+"\'");
            if(i<categoryList.size()-1){
                builder.append(",");
            }
        }
        builder.append(")");

        String selectQuery = "SELECT  * FROM " + TABLE_FEEDS + builder.toString();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                Article article = new Article();
                article.setDate(Long.valueOf(cursor.getString(1)));
                article.setContent(cursor.getString(2));
                article.setDescription(cursor.getString(3));
                article.setImage(Uri.parse(cursor.getString(4)));
                article.setTitle(cursor.getString(5));
                article.setSource(Uri.parse(cursor.getString(6)));
                article.setTags(QuickNewsUtils.csvToList(cursor.getString(7)));
                article.setCategory(cursor.getString(8));
                article.setSourceTitle(cursor.getString(9));
                articleList.add(article);

            } while (cursor.moveToNext());
        }

        return articleList;
    }

    public void deleteAllArticles(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_FEEDS);
        db.close();
    }

}
