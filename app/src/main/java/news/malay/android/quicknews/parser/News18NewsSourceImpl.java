package news.malay.android.quicknews.parser;

import android.net.Uri;
import android.text.Html;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import news.malay.android.quicknews.utils.quicknewsutils.QuickNewsUtils;

/**
 * Created by yogeshpatel on 09/09/17.
 */

public class News18NewsSourceImpl implements NewsSource {
    private final DateFormat dateFormat;
    private final XmlPullParser xmlParser;

    public News18NewsSourceImpl(){
        dateFormat = new SimpleDateFormat("EEEE,MMMM d,yyyy HH:mm a");
        dateFormat.setTimeZone(Calendar.getInstance().getTimeZone());

        // Initialize XmlPullParser object with a common configuration
        XmlPullParser parser = null;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(false);
            parser = factory.newPullParser();
        }
        catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        xmlParser = parser;
    }
    @Override
    public List<Article> parse(String rssStream,String category) {
        List<Article> articleList = new ArrayList<>();

        InputStream input = new ByteArrayInputStream(rssStream.getBytes());
        try {
            xmlParser.setInput(input, null);
            Article article = new Article();
            article.setSourceTitle(NewsSourceNames.NEWS18);
            int eventType = xmlParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = xmlParser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("item")){
                            article = new Article();
                            article.setSourceTitle(NewsSourceNames.NEWS18);
                        }
                        else{
                            handleNode(tagname,article);
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("item")) {
                            // Generate ID
                            article.setId(Math.abs(article.hashCode()));

                            // Remove content thumbnail
                            if(article.getImage() != null && article.getContent() != null) {
                                article.setContent(article.getContent().replaceFirst("<img.+?>", ""));
                            }
                            if(QuickNewsUtils.isValidArticle(article)) {
                                article.setCategory(category);
                                articleList.add(article);
                            }

                        }
                    break;
                }

                eventType = xmlParser.next();
            }

            } catch (XmlPullParserException e){
                e.printStackTrace();
            } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(articleList);
        return articleList;
    }
    /**
     * Handles a node from the tag node and assigns it to the correct article value.
     * @param tag The tag which to handle.
     * @param article Article object to assign the node value to.
     * @return True if a proper tag was given or handled. False if improper tag was given or
     * if an exception if triggered.
     */
    private void handleNode(String tag, Article article) throws IOException, XmlPullParserException {
        if(xmlParser.next() != XmlPullParser.TEXT) {
            return;
        }
        if (tag.equalsIgnoreCase("link"))
            article.setSource(Uri.parse(xmlParser.getText()));
        else if (tag.equalsIgnoreCase("title"))
            article.setTitle(xmlParser.getText());
        else if (tag.equalsIgnoreCase("description")) {
            String encoded = xmlParser.getText();
            String imageUrl = extractImageUrl(encoded);
           article.setImage(Uri.parse(imageUrl));
           encoded = QuickNewsUtils.replaceQuotesFromTheStrings(encoded);

            article.setDescription(Html.fromHtml(encoded.replaceAll("<img.+?>", "")).toString());

        }
        else if (tag.equalsIgnoreCase("pubDate")) {
            article.setDate(getParsedDate(xmlParser.getText()));
        }
    }
    private String extractImageUrl(String description) {
        String regularExpression = "src=\'(.*)\' width";
        Pattern pattern = Pattern.compile(regularExpression);
        // Now create matcher object.
        Matcher matcher = pattern.matcher(description);


        if (matcher.find( )) {
            return   matcher.group(1);
        }
        return "";
    }
    /**
     * Converts a date in the "EEEE, MMMMM dd,yyyy HH:mm a" format to a long value.
     * @param encodedDate The encoded date which to convert.
     * @return A long value for the passed date String or 0 if improperly parsed.
     */
    private long getParsedDate(String encodedDate) {
        try {
            return dateFormat.parse(dateFormat.format(dateFormat.parseObject(encodedDate))).getTime();
        }
        catch (ParseException e) {
            //  log(TAG, "Error parsing date " + encodedDate, Log.WARN);
            e.printStackTrace();
            return 0;
        }
    }

}
