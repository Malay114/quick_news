package news.malay.android.quicknews;

import java.util.List;

import news.malay.android.quicknews.parser.Article;

/**
 * Created by yogeshpatel on 02/09/17.
 */

public interface NewsLoaderCallback {

    void newsDownloadCompleted(List<Article> articleList);
    void errorOccurred();
}
