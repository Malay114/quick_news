package news.malay.android.quicknews.parser;

/**
 * Created by yogeshpatel on 02/09/17.
 */

public interface NewsSourceNames {

    String PHYS_ORG = "Phys.org";
    String HINDUSTAN_TIMES = "HindustanTimes";
    String BBC = "BBC News";
    String NEWS18 = "News18";
    String INDIA_TV_NEWS = "India TV";
    String SCIENCE_DAILY = "ScienceDaily";
    String SCIENCE_NEWS = "Science News";
    String FIRST_POST = "Firstpost";




}
