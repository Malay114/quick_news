package news.malay.android.quicknews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.Toast;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import news.malay.android.quicknews.parser.Article;
import news.malay.android.quicknews.ui.adapters.NewsSlideAdapter;
import news.malay.android.quicknews.utils.quicknewsutils.QuickNewsUtils;

/**
 * This is the first activity that will be launched soon after SplashActiity.
 * This class is responsible for showing UI for all news based on selected category.
 */
public class MainActivity extends BaseActivity {


    private ViewPager mPager;
    private NewsSlideAdapter mPagerAdapter;
    private List<Article> articleList;
    private SwipeRefreshLayout swipeContainer;
    private Set<String> selectedCategory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_slide);
        selectedCategory = sharedPref.getSelectedCategory();
        articleList = getNewsOfSelectedCategory(selectedCategory);

        if(articleList.size()<=0){
            startErrorActivity();
        }

        DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mPager = (ViewPager) findViewById(R.id.pager);
        swipeContainer = (SwipeRefreshLayout)findViewById(R.id.swiperefresh);



        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadUrls();
            }
        });

        Collections.sort(articleList);

        mPagerAdapter= new NewsSlideAdapter(getSupportFragmentManager(),articleList);
        mPager.setAdapter(mPagerAdapter);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                if(position == 0){
                    swipeContainer.setEnabled(true);
                }
                else{
                    swipeContainer.setEnabled(false);
                }
                if(position % 20 ==0) {
                    int pendingStories = articleList.size();
                    Toast.makeText(MainActivity.this,  String.valueOf(pendingStories-position) + " more stories to read", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {

            }

            @Override
            public void onDrawerOpened(View view) {

            }

            @Override
            public void onDrawerClosed(View view) {
                loadDataInViewPager();

            }

            @Override
            public void onDrawerStateChanged(int i) {
            }
        });

    }

    /**
     * This function will get news from selected category and set to the
     * ma
     */
    public void loadDataInViewPager(){
        Set<String> curSelectedCategory = sharedPref.getSelectedCategory();

        boolean isSame = QuickNewsUtils.isTwoListViewsAreSame(selectedCategory,curSelectedCategory);
        selectedCategory =new HashSet<>(curSelectedCategory); ;
        if(!isSame) {
            articleList = getNewsOfSelectedCategory(curSelectedCategory);
            Collections.sort(articleList);
            mPagerAdapter.dataSetChanged(articleList);
            mPager.setCurrentItem(0, true);
        }
    }

    @Override
    void dataLoaded() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadDataInViewPager();
                if (swipeContainer != null) {
                    swipeContainer.setRefreshing(false);
                }

            }
        });

    }
    private void startErrorActivity(){
        Intent i = new Intent(MainActivity.this, ErrorActivity.class);
        startActivity(i);
        finish();
    }

}
