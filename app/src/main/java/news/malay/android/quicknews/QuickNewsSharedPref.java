package news.malay.android.quicknews;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.ArraySet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by yogeshpatel on 11/09/17.
 */

public class QuickNewsSharedPref {
    private Context context;
    private final  String categorySet = "categorySet";
    private final  String LAST_UPDATED_TIME = "LastUpdatedTime";


    public QuickNewsSharedPref(Context context){
        this.context = context;
    }

    private SharedPreferences getSharePreferences()
    {
        return context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);

    }
    private SharedPreferences getSharePreferencesForTime()
    {
        return context.getSharedPreferences(context.getString(R.string.preference_file_time_key), Context.MODE_PRIVATE);

    }
    public Set<String> getSelectedCategory(){
        Set<String> emptySet = new HashSet<>();
        SharedPreferences sharedPreferences = getSharePreferences();
        Set<String> set = sharedPreferences.getStringSet(categorySet, emptySet);

        return set;
    }
    public void addCategory(String category){
        SharedPreferences sharedPreferences = getSharePreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Set<String> set = getSelectedCategory();
        editor.clear();
        set.add(category);
        editor.putStringSet(categorySet,set);
        editor.commit();
    }
    public void removeCategory(String category){
        SharedPreferences sharedPreferences = getSharePreferences();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Set<String> set = getSelectedCategory();
        set.remove(category);
        editor.clear();
        editor.putStringSet(categorySet,set);
        editor.commit();

    }
    public void setLastUpdatedTime(long time){
        SharedPreferences sharedPreferences = getSharePreferencesForTime();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(LAST_UPDATED_TIME,time);
        editor.commit();


    }
    public long getLastUpatedTime(){
        SharedPreferences sharedPreferences = getSharePreferencesForTime();
       return sharedPreferences.getLong(LAST_UPDATED_TIME,0);
    }
}
