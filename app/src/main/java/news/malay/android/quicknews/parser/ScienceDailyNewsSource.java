package news.malay.android.quicknews.parser;

import android.net.Uri;
import android.text.Html;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import news.malay.android.quicknews.utils.quicknewsutils.QuickNewsUtils;

/**
 * Created by yogeshpatel on 09/09/17.
 */

public class ScienceDailyNewsSource  implements NewsSource {
    private final DateFormat dateFormat;
    private final XmlPullParser xmlParser;

    public ScienceDailyNewsSource() {
        dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z",Locale.US);
        dateFormat.setTimeZone(Calendar.getInstance().getTimeZone());

        // Initialize XmlPullParser object with a common configuration
        XmlPullParser parser = null;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(false);
            parser = factory.newPullParser();
        }
        catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        xmlParser = parser;
    }

    @Override
    public List<Article> parse(String rssStream,String category) {
        List<Article> articleList = new ArrayList<>();
        try {
            // Get InputStream from String and set it to our XmlPullParser
            InputStream input = new ByteArrayInputStream(rssStream.getBytes());
            xmlParser.setInput(input, null);

            // Reuse Article object and event holder
            Article article = new Article();
            article.setSourceTitle(NewsSourceNames.SCIENCE_DAILY);
            int eventType = xmlParser.getEventType();

            // Loop through the entire xml feed
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagname = xmlParser.getName();
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagname.equalsIgnoreCase("item")) { // Start a new instance
                            article = new Article();
                            article.setSourceTitle(NewsSourceNames.SCIENCE_DAILY);
                        }
                        // Enclosures not readable as text by XmlPullParser in Android and will fail in handleNode, considered not a bug
                        // https://code.google.com/p/android/issues/detail?id=18658
                        else if (tagname.equalsIgnoreCase("media:thumbnail")) {
                            handleMediaContent(tagname, article);
                        } else // Handle this node if not an entry tag
                            if(!handleNode(tagname, article)){

                            }
                        break;
                    case XmlPullParser.END_TAG:
                        if (tagname.equalsIgnoreCase("item")) {
                            // Generate ID
                            article.setId(Math.abs(article.hashCode()));

                            // Remove content thumbnail
                            if(article.getImage() != null && article.getContent() != null)
                                article.setContent(article.getContent().replaceFirst("<img.+?>", ""));

                            // (Optional) Log a minimized version of the toString() output
                            // log(TAG, article.toShortString(), Log.INFO);

                            // Add article object to list
                            if(QuickNewsUtils.isValidArticle(article)) {
                                article.setCategory(category);
                                articleList.add(article);
                            }
                        }
                        break;
                    default:
                        break;
                }
                eventType = xmlParser.next();
            }
        }
        catch (IOException e) {
            // Uh oh
            e.printStackTrace();
        }
        catch (XmlPullParserException e) {
            // Oh noes
            e.printStackTrace();
        }
        Collections.sort(articleList);
        return articleList;
    }
    /**
     * Handles a node from the tag node and assigns it to the correct article value.
     * @param tag The tag which to handle.
     * @param article Article object to assign the node value to.
     * @return True if a proper tag was given or handled. False if improper tag was given or
     * if an exception if triggered.
     */
    private boolean handleNode(String tag, Article article) {
        try {
            if(xmlParser.next() != XmlPullParser.TEXT) {

                return false;
            }


            if (tag.equalsIgnoreCase("link"))
                article.setSource(Uri.parse(xmlParser.getText()));
            else if (tag.equalsIgnoreCase("title"))
                article.setTitle(xmlParser.getText());
            else if (tag.equalsIgnoreCase("description")) {
                String encoded = xmlParser.getText();
                encoded = QuickNewsUtils.replaceQuotesFromTheStrings(encoded);
                article.setDescription(Html.fromHtml(encoded.replaceAll("<img.+?>", "")).toString());
            }
            else if (tag.equalsIgnoreCase("content:encoded"))
                article.setContent(xmlParser.getText().replaceAll("[<](/)?div[^>]*[>]", ""));
            else if (tag.equalsIgnoreCase("category"))
                article.setNewTag(xmlParser.getText());
            else if (tag.equalsIgnoreCase("pubDate")) {
                article.setDate(getParsedDate(xmlParser.getText()));
            }

            return true;
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        catch (XmlPullParserException e) {
            e.printStackTrace();
            return false;
        }
    }
    /**
     * Parses the media content of the entry
     * @param tag The tag which to handle.
     * @param article Article object to assign the node value to.
     */
    private void handleMediaContent(String tag, Article article) {
        String url = xmlParser.getAttributeValue(null, "url");
        if(url == null) {
            throw new IllegalArgumentException("Url argument must not be null");
        }

        article.setImage(Uri.parse(url));
    }

    /**
     * Converts a date in the "EEE, d MMM yyyy HH:mm:ss Z" format to a long value.
     * @param encodedDate The encoded date which to convert.
     * @return A long value for the passed date String or 0 if improperly parsed.
     */
    private long getParsedDate(String encodedDate) {
        try {
            return dateFormat.parse(dateFormat.format(dateFormat.parseObject(encodedDate))).getTime();
        }
        catch (ParseException e) {
            //  log(TAG, "Error parsing date " + encodedDate, Log.WARN);
            e.printStackTrace();
            return 0;
        }
    }


}
