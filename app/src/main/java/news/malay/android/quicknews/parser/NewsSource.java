package news.malay.android.quicknews.parser;

import java.util.List;

/**
 * Created by yogeshpatel on 02/09/17.
 */

public interface NewsSource {

    List<Article> parse(String xmlData,String category);

}
