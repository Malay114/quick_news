package news.malay.android.quicknews.utils.quicknewsutils;

import android.net.Uri;
import android.util.Log;
import android.util.Patterns;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import news.malay.android.quicknews.parser.Article;
import news.malay.android.quicknews.parser.NewsSourceNames;

/**
 * Created by yogeshpatel on 05/09/17.
 */

public class QuickNewsUtils {

    public static boolean isValidString(String data) {
        if (data == null || data.length() <= 0) return false;

        return true;
    }

    public static boolean isValidURI(Uri uri) {

        if (uri == null || !isValidString(uri.toString())) return false;

        return Patterns.WEB_URL.matcher(uri.toString()).matches();

    }

    public static boolean isValidSourceTitle(String sourceTitle) {

        if (!isValidString(sourceTitle)) return false;

        return true;
    }

    public static boolean isValidDate(long timeInMillis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss Z");
        Date date = new Date(timeInMillis);

        Calendar cal = Calendar.getInstance();
        cal.setLenient(false);
        cal.setTime(date);
        try {
            cal.getTime();
            return true;
        }
        catch (Exception e) {
        Log.e("Quick News Utils","Invalid date");
            return false;
        }
    }




    public static boolean isValidArticle(Article article){

       // Log.e("article",article.toJson());

        if(!isValidURI(article.getImage())) {
            Log.e("Validation","Invalid Image ");
            return false;
        }
        if(!isValidSourceTitle(article.getSourceTitle())){
            Log.e("Validation","Invalid source title.");

            return false;
        }
        if(!isValidString(article.getTitle())) {
            Log.e("Validation","Return false 3");

            return false;
        }
        if(!isValidDate(article.getDate())) {
            Log.e("Validation","Return false 4");

            return false;
        }
        if(!isValidString(article.getDescription())) {
            Log.e("Validation", "Return true 5");
            return  false;
        }

        return true;

    }

    public static String convertTodayOrYesterday(long timeInMillis) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

            //Date date = new Date();
            //date.setTime(timeInMillis);
            Calendar givenDate = Calendar.getInstance();
            //givenDate.setTime(date);
            givenDate.setTimeInMillis(timeInMillis);

            Calendar now = Calendar.getInstance();

            if (now.get(Calendar.DATE) == givenDate.get(Calendar.DATE)) {
                return "Today ";
            } else if (now.get(Calendar.DATE) - givenDate.get(Calendar.DATE) == 1) {
                return "Yesterday ";
            } else {

                return formatter.format(givenDate.getTime());

            }
        }catch (Exception e){
            return "";
        }
    }
    public static String stringListToString(List<String> stringList){

        StringBuilder stringBuilder = new StringBuilder();
        for(String str : stringList){
            stringBuilder.append(str).append(",");
        }
        return stringBuilder.toString();
    }

    public static List<String> csvToList(String csvStr){

        String [] strings = csvStr.split(",");
        List list = (List) Arrays.asList(strings);
        return list;
    }
    public static String replaceQuotesFromTheStrings(String data){
        String encoded = data;
        encoded = encoded.replaceAll("”","&quot; ");
        encoded = encoded.replaceAll("“","&quot; ");
        encoded = encoded.replaceAll("’","&apos; ");
        encoded = encoded.replaceAll("‘","&apos; ");


        return encoded;
    }
    public static String formateDateToView(long timeInMillis){
        long unixTime = timeInMillis;
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a");
        Date date = new Date(unixTime);

        return formatter.format(date);

    }

    public static long dateDiffWithCurDateInHours(Date lastUpdatedDate,Date currentDate){
        long diff = currentDate.getTime() - lastUpdatedDate.getTime();
        long diffHours = diff / (60 * 60 * 1000);

        return diffHours;
    }

    public static String newsSourceName(String title){
        if(title.toLowerCase().contains(NewsSourceNames.PHYS_ORG.toLowerCase())){
            return NewsSourceNames.PHYS_ORG;
        }
        else if(title.toLowerCase().contains(NewsSourceNames.HINDUSTAN_TIMES.toLowerCase())){
            return NewsSourceNames.HINDUSTAN_TIMES;
        }
        else if(title.toLowerCase().contains(NewsSourceNames.BBC.toLowerCase())){
            return NewsSourceNames.BBC;
        }
        else if(title.toLowerCase().contains(NewsSourceNames.NEWS18.toLowerCase())) {
            return NewsSourceNames.NEWS18;
        }
        else if(title.toLowerCase().contains(NewsSourceNames.INDIA_TV_NEWS.toLowerCase())){
            return NewsSourceNames.INDIA_TV_NEWS;
        }
        else if(title.toLowerCase().contains(NewsSourceNames.FIRST_POST.toLowerCase())){
            return NewsSourceNames.FIRST_POST;
        }
        else if(title.toLowerCase().contains(NewsSourceNames.SCIENCE_DAILY.toLowerCase())){
            return NewsSourceNames.SCIENCE_DAILY;
        }
        return "";
    }

    public static boolean isTwoListViewsAreSame(Set<String> listA, Set<String> listB){
       return listA.size() == listB.size() && listA.containsAll(listB) && listB.containsAll(listA);

    }

}
